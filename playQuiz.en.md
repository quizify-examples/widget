# **Widget PlayQuiz**

## Table of Contents

- [Start](#introduction)
- [Parameters](#parameters)
- [Example](#usage)
- [Playing the quiz](#playing)

## Start <a name="installation"></a>

To use the Quizify Widget, copy and paste the following snippet into the `<body/>` tag of the .html file in which you want to use it.

```html
<div
  id="play-quiz-widget"
  data-examid="1"
  data-lang="es"
  data-ongoback="https://gpt.quizify.com/"
  data-darkmode="false"
  data-enablesound="true"
></div>

<script type="module" src="https://widget.quizify.com/playQuiz.js"></script>
```

<br>

## Parameters <a name="parameters"></a>

### _id_

It must be equal to "play-quiz-widget", otherwise the widget as such will not work.

```bash
id="play-quiz-widget"
```

<br>

### _data-examid_

Must be a number that represents the quiz id.

```bash
 data-examid="1" // example
```

<br>

### _data-lang_

Represents the language of the widget interface. Two languages are available: English and Spanish.

```bash
 data-lang="es" // Spanish
 data-lang="en" // English
```

<br>

### _data-ongoback_

Represents the url to which the user can return once he/she has finished the Quiz or wants to cancel it.

```bash
 data-ongoback="https://dev.quiz-gpt.com/"  // ejemplo
```

<br>

### _data-darkmode_

Enables light/dark mode.

```bash
 data-darkmode="false" // modo claro
 data-darkmode="true" // mode oscuro
```

<br>

### _data-enablesound_

Enables the Quiz sound.

```bash
 data-enablesound="true" // sonido habilitado
 data-enablesound="false" // sonido deshabilitado
```

<br>

## Example <a name="usage"></a>

<br>

index.html

```html
<!-- optional styles -->
<style>
  /* si quieres desaparecer los margenes */
  body {
    margin: 0;
    padding: 0;
  }

  /* si quieres desaparecer la barra de scroll */
  body::-webkit-scrollbar {
    width: 0;
  }
</style>
<!-- estilos opcionales -->

<body>
  <div
    id="play-quiz-widget"
    data-examid="1"
    data-lang="es"
    data-ongoback="https://gpt.quizify.com/"
    data-darkmode="false"
    data-enablesound="true"
  ></div>

  <script type="module" src="https://widget.quizify.com/main.js"></script>
</body>
```

<br>

## Playing the quiz <a name="playing"></a>

![Alt Text](screenshot.png)
