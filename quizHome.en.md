# **Widget QuizHome**

## Table of Contents

- [Start](#introduction)
- [Parameters](#parameters)
- [Example](#usage)
- [Playing the quiz](#playing)

## Start <a name="installation"></a>

To use the Quizify Widget, copy and paste the following snippet into the `<body/>` tag of the .html file in which you want to use it.

```html
<div
  id="quiz-home-widget"
  data-lang="es"
  data-darkmode="false"
  data-enablesound="true"
></div>

<script type="module" src="https://widget.quizify.com/quizHome.js"></script>
```

<br>

## Attributes <a name="parameters"></a>

<br>

### _data-lang_

Represents the language of the widget interface. Two languages are available: English and Spanish.

```bash
 data-lang="es" // Spanish
 data-lang="en" // English
```

<br>

### _data-darkmode_

Enables light/dark mode.

```bash
 data-darkmode="false" // modo claro
 data-darkmode="true" // mode oscuro
```

<br>

### _data-enablesound_

Enables the Quiz sound.

```bash
 data-enablesound="true" // sonido habilitado
 data-enablesound="false" // sonido deshabilitado
```

<br>

## Example <a name="usage"></a>

<br>

index.html

```html
<!-- optional styles -->
<style>
  /* Hide margins */
  body {
    margin: 0;
    padding: 0;
  }

  /* Hide scroll bar */
  body::-webkit-scrollbar {
    width: 0;
  }
</style>
<!-- optional styles -->

<body>
  <div
    id="quiz-home-widget"
    data-lang="es"
    data-darkmode="false"
    data-enablesound="true"
  ></div>

  <script type="module" src="https://widget.quizify.com/quizHome.js"></script>
</body>
```

<br>

## Playing the quiz <a name="playing"></a>

![Alt Text](quizHome.png)
