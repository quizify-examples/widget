# **Widget PlayQuiz**

## Tabla de contenido

- [Comenzar](#introduction)
- [Attributos](#parameters)
- [Ejemplo](#usage)
- [Jugando la Quiz](#playing)

## Comenzar <a name="installation"></a>

Para utilizar el Widget de Quizify, copie y pegue el siguiente snippet dentro de la etiqueta `<body/>` del archivo .html en el cual usted quiere usarlo.

```html
<div
  id="play-quiz-widget"
  data-examid="1"
  data-lang="es"
  data-ongoback="https://gpt.quizify.com/"
  data-darkmode="false"
  data-enablesound="true"
></div>

<script type="module" src="https://widget.quizify.com/playQuiz.js"></script>
```

<br>

## Atributos <a name="parameters"></a>

### _id_

Es necesario que sea igual a "play-quiz-widget", sino el widget como tal no funcionará.

```bash
id="play-quiz-widget"
```

<br>

### _data-examid_

Debe ser un número que representa el id de la quiz.

```bash
 data-examid="1" // ejemplo
```

<br>

### _data-lang_

Representa el lenguaje de la interfaz del widget. Hay dos lenguajes disponibles: Español e Inglés.

```bash
 data-lang="es" // Español
 data-lang="en" // Inglés
```

<br>

### _data-ongoback_

Representa la url a la cual el usuario puede regresar una vez que haya terminado la Quiz o quiera cancelarla.

```bash
 data-ongoback="https://dev.quiz-gpt.com/"  // ejemplo
```

<br>

### _data-darkmode_

Habilita el modo claro/oscuro.

```bash
 data-darkmode="false" // modo claro
 data-darkmode="true" // mode oscuro
```

<br>

### _data-enablesound_

Habilita el sonido de la Quiz.

```bash
 data-enablesound="true" // sonido habilitado
 data-enablesound="false" // sonido deshabilitado
```

<br>

## Ejemplo <a name="usage"></a>

<br>

index.html

```html
<!-- estilos opcionales -->
<style>
  /* si quieres desaparecer los margenes */
  body {
    margin: 0;
    padding: 0;
  }

  /* si quieres desaparecer la barra de scroll */
  body::-webkit-scrollbar {
    width: 0;
  }
</style>
<!-- estilos opcionales -->

<body>
  <div
    id="play-quiz-widget"
    data-examid="1"
    data-lang="es"
    data-ongoback="https://gpt.quizify.com/"
    data-darkmode="false"
    data-enablesound="true"
  ></div>

  <script type="module" src="https://widget.quizify.com/main.js"></script>
</body>
```

<br>

## Jugando la Quiz <a name="playing"></a>

![Alt Text](screenshot.png)
